-- Create a Postgres database from a set of CSV files.
-- Those CSV files, in turn, can be created with a Python script 
-- converting JSON data to a flatter representation.

drop table if exists business cascade;
drop table if exists "user" cascade;
drop table if exists friend cascade;
drop table if exists business_category cascade;
drop table if exists category cascade;
drop table if exists review cascade;
drop table if exists tip cascade;

create table if not exists business
(
    business_id INTEGER primary key,
    full_address VARCHAR(500),
    city VARCHAR(200),
    state VARCHAR(2),
    open BOOLEAN,
    name VARCHAR(200),
    review_count integer,
    latitude float,
    longitude float,
    stars float,
    wifi VARCHAR(50),
    alcohol VARCHAR(50),
    credit_cards boolean,
    good_for_kids boolean,
    good_for_groups boolean,
    price_range integer,
    c_sunday_0 integer,
    c_sunday_1 integer,
    c_sunday_2 integer,
    c_sunday_3 integer,
    c_sunday_4 integer,
    c_sunday_5 integer,
    c_sunday_6 integer,
    c_sunday_7 integer,
    c_sunday_8 integer,
    c_sunday_9 integer,
    c_sunday_10 integer,
    c_sunday_11 integer,
    c_sunday_12 integer,
    c_sunday_13 integer,
    c_sunday_14 integer,
    c_sunday_15 integer,
    c_sunday_16 integer,
    c_sunday_17 integer,
    c_sunday_18 integer,
    c_sunday_19 integer,
    c_sunday_20 integer,
    c_sunday_21 integer,
    c_sunday_22 integer,
    c_sunday_23 integer,
    c_monday_0 integer,
    c_monday_1 integer,
    c_monday_2 integer,
    c_monday_3 integer,
    c_monday_4 integer,
    c_monday_5 integer,
    c_monday_6 integer,
    c_monday_7 integer,
    c_monday_8 integer,
    c_monday_9 integer,
    c_monday_10 integer,
    c_monday_11 integer,
    c_monday_12 integer,
    c_monday_13 integer,
    c_monday_14 integer,
    c_monday_15 integer,
    c_monday_16 integer,
    c_monday_17 integer,
    c_monday_18 integer,
    c_monday_19 integer,
    c_monday_20 integer,
    c_monday_21 integer,
    c_monday_22 integer,
    c_monday_23 integer,
    c_tuesday_0 integer,
    c_tuesday_1 integer,
    c_tuesday_2 integer,
    c_tuesday_3 integer,
    c_tuesday_4 integer,
    c_tuesday_5 integer,
    c_tuesday_6 integer,
    c_tuesday_7 integer,
    c_tuesday_8 integer,
    c_tuesday_9 integer,
    c_tuesday_10 integer,
    c_tuesday_11 integer,
    c_tuesday_12 integer,
    c_tuesday_13 integer,
    c_tuesday_14 integer,
    c_tuesday_15 integer,
    c_tuesday_16 integer,
    c_tuesday_17 integer,
    c_tuesday_18 integer,
    c_tuesday_19 integer,
    c_tuesday_20 integer,
    c_tuesday_21 integer,
    c_tuesday_22 integer,
    c_tuesday_23 integer,
    c_wednesday_0 integer,
    c_wednesday_1 integer,
    c_wednesday_2 integer,
    c_wednesday_3 integer,
    c_wednesday_4 integer,
    c_wednesday_5 integer,
    c_wednesday_6 integer,
    c_wednesday_7 integer,
    c_wednesday_8 integer,
    c_wednesday_9 integer,
    c_wednesday_10 integer,
    c_wednesday_11 integer,
    c_wednesday_12 integer,
    c_wednesday_13 integer,
    c_wednesday_14 integer,
    c_wednesday_15 integer,
    c_wednesday_16 integer,
    c_wednesday_17 integer,
    c_wednesday_18 integer,
    c_wednesday_19 integer,
    c_wednesday_20 integer,
    c_wednesday_21 integer,
    c_wednesday_22 integer,
    c_wednesday_23 integer,
    c_thursday_0 integer,
    c_thursday_1 integer,
    c_thursday_2 integer,
    c_thursday_3 integer,
    c_thursday_4 integer,
    c_thursday_5 integer,
    c_thursday_6 integer,
    c_thursday_7 integer,
    c_thursday_8 integer,
    c_thursday_9 integer,
    c_thursday_10 integer,
    c_thursday_11 integer,
    c_thursday_12 integer,
    c_thursday_13 integer,
    c_thursday_14 integer,
    c_thursday_15 integer,
    c_thursday_16 integer,
    c_thursday_17 integer,
    c_thursday_18 integer,
    c_thursday_19 integer,
    c_thursday_20 integer,
    c_thursday_21 integer,
    c_thursday_22 integer,
    c_thursday_23 integer,
    c_friday_0 integer,
    c_friday_1 integer,
    c_friday_2 integer,
    c_friday_3 integer,
    c_friday_4 integer,
    c_friday_5 integer,
    c_friday_6 integer,
    c_friday_7 integer,
    c_friday_8 integer,
    c_friday_9 integer,
    c_friday_10 integer,
    c_friday_11 integer,
    c_friday_12 integer,
    c_friday_13 integer,
    c_friday_14 integer,
    c_friday_15 integer,
    c_friday_16 integer,
    c_friday_17 integer,
    c_friday_18 integer,
    c_friday_19 integer,
    c_friday_20 integer,
    c_friday_21 integer,
    c_friday_22 integer,
    c_friday_23 integer,
    c_saturday_0 integer,
    c_saturday_1 integer,
    c_saturday_2 integer,
    c_saturday_3 integer,
    c_saturday_4 integer,
    c_saturday_5 integer,
    c_saturday_6 integer,
    c_saturday_7 integer,
    c_saturday_8 integer,
    c_saturday_9 integer,
    c_saturday_10 integer,
    c_saturday_11 integer,
    c_saturday_12 integer,
    c_saturday_13 integer,
    c_saturday_14 integer,
    c_saturday_15 integer,
    c_saturday_16 integer,
    c_saturday_17 integer,
    c_saturday_18 integer,
    c_saturday_19 integer,
    c_saturday_20 integer,
    c_saturday_21 integer,
    c_saturday_22 integer,
    c_saturday_23 integer
);

create table if not exists "user"
(
    user_id integer primary key,
    yelping_since date,
    funny_votes integer,
    useful_votes integer,
    cool_votes integer,
    review_count integer,
    name varchar(100),
    fans integer,
    average_stars float,
    "type" varchar(50),
    years_elite integer
);

create table if not exists category
(
    category_id integer primary key,
    name varchar(100)
);

create table if not exists friend
(
    friend_id integer primary key,
    user1_id integer references "user"(user_id),
    user2_id integer references "user"(user_id)
);

create index friend_user1_id on friend using btree(user1_id);
create index friend_user2_id on friend using btree(user2_id);

create table if not exists business_category
(
    business_category_id integer primary key,
    business_id integer references business(business_id),
    category_id integer references category(category_id)
);

create index business_category_business_id on business_category using btree(business_id);
create index business_category_category_id on business_category using btree(category_id);

create table if not exists review
(
    review_id integer primary key,
    user_id integer references "user"(user_id),
    business_id integer references business(business_id),
    stars float,
    date date,
    funny_votes integer,
    useful_votes integer,
    cool_votes integer
);

create index review_user_id on review using btree(user_id);
create index review_business_id on review using btree(business_id);

create table if not exists tip
(
    tip_id integer primary key,
    user_id integer references "user"(user_id),
    business_id integer references business(business_id),
    date date,
    num_likes integer
);

create index tip_user_id on tip using btree(user_id);
create index tip_business_id on tip using btree(business_id)

