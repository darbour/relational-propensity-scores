#!/usr/bin/env python
# Converts the JSON-based Yelp dataset to CSV files
# which can be readily loaded into Postgres

from __future__ import print_function
import json
import csv
import sys
from collections import defaultdict

def convert_users(source_handle, target_handle):
    """ Converts user records in source_handle to 
        CSV rows in target_handle. 

        Returns a map from ugly user ID to an integer key.
    """
    print("Converting users")
    id_map = dict()
    next_uid = 1

    csv_writer = csv.writer(target_handle, delimiter=",")
    header = ["user_id", "yelping_since", "funny_votes", "useful_votes", 
              "cool_votes", "review_count", "name", 
              "fans", "average_stars", "type", "years_elite"]

    csv_writer.writerow(header)
    for line in source_handle:
        user = json.loads(line)
        row = [next_uid, user["yelping_since"], user["votes"]["funny"], 
                user["votes"]["useful"], user["votes"]["cool"],
                user["review_count"], user["name"], user["fans"],
                user["average_stars"], user["type"], len(user["elite"])]
        ascii_row = [unicode(s).encode("ascii", "ignore") for s in row]
        id_map[user["user_id"]] = next_uid
        next_uid += 1
        if next_uid % 100 == 0:
            print("\tWriting record {0}".format(next_uid), end="\r")
            sys.stdout.flush()
        csv_writer.writerow(ascii_row)

    print()
    return id_map

def convert_friends(source_handle, target_handle, id_map):
    """ Converts the friend relation from source_handle to
        CSV rows in target_handle. id_map stores user ID translations.
    """
    print("Converting friends")

    csv_writer = csv.writer(target_handle, delimiter=",")
    friend_id = 1

    # symmetric (we'll have a record for each direction)
    header = ["friend_id", "user1_id", "user2_id"]
    csv_writer.writerow(header)
    for line in source_handle:
        user = json.loads(line)
        for friend in user["friends"]:
            row = [friend_id, id_map[user["user_id"]], id_map[friend]]
            csv_writer.writerow(row)
            friend_id += 1
            if friend_id % 100 == 0:
                print("\tWriting record {0}".format(friend_id), end="\r")
                sys.stdout.flush()
    print()

def convert_neighborhoods(source_handle, n_handle, n_xref_handle, id_map):
    """ Converts neighborhoods and the business-neighborhood relationship.

        Keyword arguments:
            source_handle -- A file containing business JSON
            n_handle -- A CSV file to write unique neighborhoods to
            n_xref_handle -- A CSV file to write neighborhood-business relationships to
            id_map -- Stores a map from encrypted business ID to business pk

        Returns a map from category name to category ID
    """
    print("Converting neighborhoods")
    n_writer = csv.writer(n_handle, delimiter=",")
    xref_writer = csv.writer(n_xref_handle, delimiter=",")
    n_id_map = dict()

    known_neighborhoods = set()

    next_n_id = 1
    next_xref_id = 1

    xref_header = ["business_neighborhood_id", "business_id", "neighborhood_id"]
    xref_writer.writerow(xref_header)

    n_header = ["neighborhood_id", "name"]
    n_writer.writerow(n_header)

    for line in source_handle:
        business = json.loads(line)
        business_id = id_map[business["business_id"]]
        for neighborhood in business["neighborhoods"]:
            if not neighborhood in known_neighborhoods:
                n_writer.writerow([next_n_id, neighborhood])
                n_id_map[neighborhood] = next_n_id
                next_n_id += 1
                known_neighborhoods.add(neighborhood)

            neighborhood_id = n_id_map[neighborhood]

            xref_writer.writerow([next_xref_id, business_id, neighborhood_id])
            next_xref_id += 1
            if next_xref_id % 100 == 0:
                print("\tWriting record{0}".format(next_xref_id), end="\r")

    print()
    return n_id_map


def convert_categories(source_handle, cat_handle, cat_xref_handle, id_map):
    """ Converts categories and the business-category relationship.

        Keyword arguments:
            source_handle -- A file containing business JSON
            cat_handle -- A CSV file to write unique categories to
            cat_xref_handle -- A CSV file to write category-business relationships to
            id_map -- Stores a map from encrypted business ID to business PK

        Returns a map from category name to category ID
    """
    print("Converting categories")
    cat_writer = csv.writer(cat_handle, delimiter=",")
    xref_writer = csv.writer(cat_xref_handle, delimiter=",")
    cat_id_map = dict()

    known_categories = set()

    next_cat_id = 1
    next_xref_id = 1

    xref_header = ["business_category_id", "business_id", "category_id"]
    xref_writer.writerow(xref_header)

    cat_header = ["category_id", "name"]
    cat_writer.writerow(cat_header)

    for line in source_handle:
        business = json.loads(line)
        business_id = id_map[business["business_id"]]
        for category in business["categories"]:
            if not category in known_categories:
                cat_writer.writerow([next_cat_id, category])
                cat_id_map[category] = next_cat_id
                next_cat_id += 1
                known_categories.add(category)

            category_id = cat_id_map[category]

            xref_writer.writerow([next_xref_id, business_id, category_id])
            next_xref_id += 1
            if next_xref_id % 100 == 0:
                print("\tWriting record {0}".format(next_xref_id), end="\r")

    print()
    return cat_id_map

def get_checkins(source_handle):
    """ Loads checkin information into memory and indexes by encrypted business ID.
        Returns a map from business to checkin information for that business.
    """
    checkins = defaultdict(lambda: defaultdict(int))
    for line in source_handle:
        content = json.loads(line)
        checkins[content["business_id"]] = defaultdict(int, content["checkin_info"])
    return checkins

def convert_businesses(source_handle, target_handle, checkins):
    """ Converts business information from source_handle to CSV
        rows in target_handle. 

        Keyword arguments:
            source_handle -- A file reference to the JSON file containing business data
            target_handle -- A file reference to store CSV results in.
            checkins -- A map from business ID to checkin information

        Returns a map from Yelp's key to an integer pk.
    """
    print("Converting businesses")
    csv_writer = csv.writer(target_handle, delimiter=",")
    id_map = dict()
    next_bid = 1

    header = ["business_id", "full_address",
                "city", "state", "open", "name", 
                "review_count", "latitude", "longitude", "stars",
                "wifi", "alcohol", "credit_cards", 
                "good_for_kids", "good_for_groups", "price_range"]
    for day_index, day in enumerate(["sunday", "monday", "tuesday", 
                "wednesday", "thursday", "friday", "saturday"]):
        for hour in range(0, 24):
            header.append("c_{0}_{1}".format(day, hour))
    csv_writer.writerow(header)

    for line in source_handle:
        business = json.loads(line)
        row = [next_bid, business["full_address"], business["city"], 
                business["state"], 
                business["open"], business["name"],
                business["review_count"], business["latitude"],
                business["longitude"], business["stars"],
                business["attributes"].get("Wi-Fi"),
                business["attributes"].get("Alcohol"),
                business["attributes"].get("Accepts Credit Cards"),
                business["attributes"].get("Good for Kids"),
                business["attributes"].get("Good for Groups"),
                business["attributes"].get("Price Range")]

        for day_index, day in enumerate(["sunday", "monday", "tuesday", 
                            "wednesday", "thursday", "friday", "saturday"]):
            for hour in range(0, 24):
                key = "{0}-{1}".format(hour, day_index)
                num_checkins = checkins[business["business_id"]][key]
                row.append(num_checkins)

        id_map[business["business_id"]] = next_bid
        next_bid += 1

        ascii_row = [unicode(s).encode("ascii", "ignore") for s in row]
        csv_writer.writerow(ascii_row)
        if next_bid % 100 == 0:
            print("\tWriting record {0}".format(next_bid), end="\r")
            sys.stdout.flush()

    print()
    return id_map

def convert_reviews(source_handle, target_handle, user_id_map, business_id_map):
    """ Converts reviews from JSON to CSV.

        Keyword arguments:
            source_handle -- A file containing JSON review data
            target_handle -- The file handle to write CSV data to
            user_id_map -- Maps encrypted user ID to user PK
            business_id_map -- Maps encrypted business Id to business PK

        Returns a map from encrypted review ID to review PK.
    """
    print("Converting reviews")
    review_writer = csv.writer(target_handle, delimiter=",")

    review_id_map = dict()
    next_review_id = 1

    review_header = ["review_id", "user_id", "business_id", "stars", "date", 
            "funny_votes", "useful_votes", "cool_votes"]
    review_writer.writerow(review_header)
    for line in source_handle:
        review = json.loads(line)

        row = [next_review_id, user_id_map[review["user_id"]], business_id_map[review["business_id"]],
                    review["stars"], review["date"], review["votes"]["funny"], 
                    review["votes"]["useful"], review["votes"]["cool"]]
        review_id_map[review["review_id"]] = next_review_id
        review_writer.writerow(row)

        next_review_id += 1
        if next_review_id % 100 == 0:
            print("\tWriting record {0}".format(next_review_id), end="\r")
            sys.stdout.flush()

    print()
    return review_id_map

def convert_tips(source_handle, target_handle, user_id_map, business_id_map):
    """ Converts tips from JSON to CSV.

        Keyword arguments:
            source_handle -- The file containing JSON data to 
            target_handle -- The file to store CSV data in
            user_id_map -- Maps encrypted user ID to user PK
            business_id_map -- Maps encrypted business ID to business Pk

        Returns a map from encrypted tip ID to tip PK.
    """
    print("Converting tips")
    tip_writer = csv.writer(target_handle)

    tip_id_map = dict()
    next_tip_id = 1

    tip_header = ["tip_id", "user_id", "business_id", "date", "num_likes"]
    tip_writer.writerow(tip_header)
    for line in source_handle:
        tip = json.loads(line)

        row = [next_tip_id, user_id_map[tip["user_id"]], business_id_map[tip["business_id"]],
                    tip["date"], tip["likes"]]
        tip_writer.writerow(row)

        next_tip_id += 1
        if next_tip_id % 100 == 0:
            print("\tWriting record {0}".format(next_tip_id), end="\r")
            sys.stdout.flush()

    print()
    return tip_id_map


with open("yelp_academic_dataset_checkin.json", "r") as source:
    checkins = get_checkins(source)

with open("yelp_academic_dataset_business.json", "r") as source:
    with open("businesses.csv", "w+") as target:
        business_id_map = convert_businesses(source, target, checkins)

with open("yelp_academic_dataset_business.json", "r") as source:
    with open("categories.csv", "w+") as chandle:
        with open("business_categories.csv", "w+") as c_xref_handle:
            convert_categories(source, chandle, c_xref_handle, business_id_map)

with open("yelp_academic_dataset_business.json", "r") as source:
    with open("neighborhood.csv", "w+") as nhandle:
        with open("business_neighborhood.csv", "w+") as n_xref_handle:
            convert_neighborhoods(source, nhandle, n_xref_handle, business_id_map)

with open("yelp_academic_dataset_user.json", "r") as source:
    with open("users.csv", "w+") as target:
        user_id_map = convert_users(source, target)

with open("yelp_academic_dataset_tip.json", "r") as source:
    with open("tips.csv", "w+") as target:
        convert_tips(source, target, user_id_map, business_id_map)

with open("/share/scratch/data/yelp/yelp_academic_dataset_user.json", "r") as source:
    with open("friends.csv", "w+") as target:
        convert_friends(source, target, user_id_map)

with open("/share/scratch/data/yelp/yelp_academic_dataset_review.json", "r") as source:
    with open("reviews.csv", "w+") as target:
        convert_reviews(source, target, user_id_map, business_id_map)


