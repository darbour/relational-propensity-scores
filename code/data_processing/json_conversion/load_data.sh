
psql yelp -c "delete from tip"
psql yelp -c "delete from review"
psql yelp -c "delete from business_category"
psql yelp -c "delete from category"
psql yelp -c "delete from friend"
psql yelp -c "delete from \"user\""
psql yelp -c "delete from business"

psql yelp -c "\copy business from 'csv/businesses.csv' with csv header delimiter as ',' null as 'None';"
psql yelp -c "\copy \"user\" from 'csv/users.csv' with csv header delimiter as ',' null as 'None';"
psql yelp -c "\copy friend from 'csv/friends.csv' with csv header delimiter as ',' null as 'None';"
psql yelp -c "\copy category from 'csv/categories.csv' with csv header delimiter as ',' null as 'None';"
psql yelp -c "\copy business_category from 'csv/business_categories.csv' with csv header delimiter as ',' null as 'None';"
psql yelp -c "\copy review from 'csv/reviews.csv' with csv header delimiter as ',' null as 'None';"
psql yelp -c "\copy tip from 'csv/tips.csv' with csv header delimiter as ',' null as 'None';"

