trial=${1:-t0}
marginal_distr=${2:-mixture}

echo $trial $marginal_distr $error

psql yelp -c"DROP TABLE IF EXISTS ${trial}_business CASCADE"
psql yelp -c"DROP TABLE IF EXISTS ${trial}_city CASCADE"
psql yelp -c"DROP TABLE IF EXISTS ${trial}_category CASCADE"

# hallucinate business values
echo "Creating table ${trial}_business"
psql yelp -A -t -c "select business_id from business" | Rscript generateMarginal.R 2 $marginal_distr > ${trial}_business_vars.csv
psql yelp -c"CREATE TABLE ${trial}_business( business_id int primary key, B1 numeric, B2 numeric);"
psql yelp -c"\copy ${trial}_business from '${trial}_business_vars.csv' delimiter ',' csv;"

# hallucinate city values
echo "Creating table ${trial}_city"
psql yelp -A -t -c "select distinct city from business" | Rscript generateMarginal.R 2 $marginal_distr > ${trial}_city_vars.csv
psql yelp -c"CREATE TABLE ${trial}_city( city varchar(50) primary key, L1 numeric, L2 numeric);"
psql yelp -c"\copy ${trial}_city from '${trial}_city_vars.csv' delimiter ',' csv;"

# hallucinate category values
echo "Creating table ${trial}_category"
psql yelp -A -t -c "select category_id from category" | Rscript generateMarginal.R  2 $marginal_distr > ${trial}_category_vars.csv
psql yelp -c"CREATE TABLE ${trial}_category( category int primary key, C1 numeric, C2 numeric);"
psql yelp -c"\copy ${trial}_category from '${trial}_category_vars.csv' delimiter ',' csv;"

# now update the aggregate business values (treatment and outcome)
echo "Updating & aggregating business values"
query="SELECT  
        ${trial}_business.business_id, 
        ${trial}_business.b1,
        ${trial}_business.b2,
        ${trial}_city.L1 as L1,
        ${trial}_city.L2 as L2,
        ${trial}_city.city as id_city,
        min(${trial}_category.c1) as min_c1, 
        max(${trial}_category.c1) as max_c1, 
        avg(${trial}_category.c1) as avg_c1,
        COALESCE(sqrt(variance(${trial}_category.c1)),0) as sd_c1,
        min(${trial}_category.c2) as min_c2, 
        max(${trial}_category.c2) as max_c2, 
        avg(${trial}_category.c2) as avg_c2,
        COALESCE(sqrt(variance(${trial}_category.c2)),0) as sd_c2,
        count(${trial}_category.*) as cat_degree
FROM ${trial}_business
    inner join business
    on business.business_id  = ${trial}_business.business_id
    inner join ${trial}_city
    on ${trial}_city.city = business.city
    inner join business_category
    on business_category.business_id = ${trial}_business.business_id
    inner join ${trial}_category
    on ${trial}_category.category = business_category.category_id
group by ${trial}_business.business_id, ${trial}_business.b1, ${trial}_business.b2, 
${trial}_city.l1, ${trial}_city.l2, ${trial}_city.city;"

psql yelp -A -c"$query" | sed '$d' > ${trial}_data.csv

echo "Getting business same city data"
query="SELECT
    src.business_id,
    min(trg.b1) as min_blb_b1,
    max(trg.b1) as max_blb_b1,
    avg(trg.b1) as avg_blb_b1,
    COALESCE(sqrt(variance(trg.b1)), 0) as sd_blb_b1,
    min(trg.b2) as min_blb_b2,
    max(trg.b2) as max_blb_b2,
    avg(trg.b2) as avg_blb_b2,
    COALESCE(sqrt(variance(trg.b2)), 0) as sd_blb_b2
from ${trial}_business src
    inner join business b1
    on src.business_id = b1.business_id
    inner join business b2
    on b1.city = b2.city
        and b1.business_id != b2.business_id
    inner join ${trial}_business trg
    on trg.business_id = b2.business_id
group by src.business_id"

psql yelp -A -c"$query" | sed '$d' > ${trial}_same_city_data.csv

echo "Getting business same category data"
query="SELECT
    src.business_id,
    min(trg.b1) as min_bcb_b1,
    max(trg.b1) as max_bcb_b1,
    avg(trg.b1) as avg_bcb_b1,
    COALESCE(sqrt(variance(trg.b1)), 0) as sd_bcb_b1,
    min(trg.b2) as min_bcb_b2,
    max(trg.b2) as max_bcb_b2,
    avg(trg.b2) as avg_bcb_b2,
    COALESCE(sqrt(variance(trg.b2)), 0) as sd_bcb_b2
from ${trial}_business src

    inner join business b1
    on src.business_id = b1.business_id

    inner join business_category bcat1
    on b1.business_id = bcat1.business_id

    inner join business b2
    on b2.business_id != b1.business_id

    inner join business_category bcat2
    on b2.business_id = bcat2.business_id
    and bcat2.category_id = bcat1.category_id

    inner join ${trial}_business trg
    on trg.business_id = b2.business_id
group by src.business_id"

psql yelp -A -c"$query" | sed '$d' > ${trial}_same_category_data.csv

# clean up after yourself!
echo "Clearing temp tables"
psql yelp -c"DROP TABLE ${trial}_business CASCADE"
psql yelp -c"DROP TABLE ${trial}_city CASCADE"
psql yelp -c"DROP TABLE ${trial}_category CASCADE"
rm *.tmp
