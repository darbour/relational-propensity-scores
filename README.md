### Dependencies ###

#### R
- optmatch
- plyr
- RItools
- glmnet
- gbm
- kernlab

### Replicating the Synthetic Results Section

#### Creating a Compiled Synthetic Dataset

Our experiments run against a Postgres representation of the Yelp data.
You can either use the supplied compiled datasets (in the *data* folder), or re-create the database and re-compile the datasets.
To do this, first navigate to *code/data_processing/json_conversion*, and run *convert_data.py*.
This file expects to find the Yelp JSON files in the same directory. You may need to update the paths to these files (towards the end of the file).
After the CSV files have been created, run *create_schema.sql* against an existing Postgres database named *yelp* to create the necessary database objects.
Finally, execute *load_data.sh* to populate the newly created SQL tables with the CSV files built by *convert_data.py*.

#### Executing the Synthetic Experiments 

The script *run_experiment.R* in the *code* directory can be used to assess 
the performance of RPSM under a variety of world/model configurations. 
Each execution of this script will use differently randomly-generated coefficients for covariates, 
so to run many trials, simply call the script many times (results will be appended to the output file).
The script takes as many as 7 arguments:

1. A CSV file containing values sampled from the marginal distributions for each covariates (generated in a previous step). The *compiled-normal-data.csv* file was used for the paper.
2. A decription of the "covariate complexity" to employ. To replicate results in the paper, sweep through values in the range [1,5].
3. A machine learning technique/algorithm to use when estimating probability of treatment (*logistic* used in the paper).
4. A world model. Corresponding to the paper labels, supply either 2-hop (World 2), 4-hop (World 4), 2-hop-latent (World 2+), 4-hop-latent (World 4+), 2-hop-mean-only (World 2-), or 4-hop-mean-only (World 4-). 
5. A model indicating the covariates to be used when computing treatment probabilities. Corresponding to the paper labels, supply either 2-hop (RPSM2), 4-hop (RPSM4), 4-hop-blocking (RPSM4+), 2-hop-blocking (RPSM2+), 4-hop-mean-only (RPSM4-), or 2-hop-mean-only (RPSM2-).
6. Standard deviation of noise/error variable to incorporate the linear formula *outcome ~ covariates + treatment + noise*. The noise level was set to 0.1 for the paper.  
7. A file to write run results to. Results are written in an append-mode to allow for execution of several runs.

For instance, to run an instance of RPSM2 in World 2, you would make a call such as:
     
```
#!sh

Rscript run_experiment.R ../data/compiled-normal-data.csv 1 logistic 2-hop 2-hop 0.1 results.csv
```

#### Analyzing the Results

The previous step will produce a CSV file describing the world/model configuration and the results in each scenario.
Then, use *create_plots.R* (possibly updating the results file name at the top of the file) to generate a series of 
PDF files corresponding to those used in the paper.


### Replicating the Yelp Hypotheses section
In the yelp_experiments directory the script parse_businss_avg_rating_causes_open.R will replicated all results found in the challenge submission. 

The script requires a csv file of the same name. That file was is the result of running the SQL file "business_avg_rating_causes_open.sql" on a Postgres database created from the Yelp challenge dataset JSON files.