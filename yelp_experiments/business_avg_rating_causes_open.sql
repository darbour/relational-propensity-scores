
SELECT
    business.business_id,
    business.city,
    business.state,
    business.stars,
    business.credit_cards,
    business.alcohol, 
    business.wifi,
    business.price_range,
    business.review_count,
    business.open,
    avg(review.useful_votes) AS bru_avg_useful_votes,
    min(review.useful_votes) AS bru_min_useful_votes,      
    max(review.useful_votes) AS bru_max_useful_votes,
    avg(yelper.review_count) AS avg_yelper_review_count,
    min(yelper.review_count) AS min_yelper_review_count, 
    max(yelper.review_count) AS max_yelper_review_count,
    avg(yelper.average_stars) AS avg_yelper_avg_stars,
    min(yelper.average_stars) AS min_yelper_avg_stars,
    max(yelper.average_stars) AS max_yelper_avg_stars,
    avg(yelper.useful_votes) AS avg_yelper_useful_votes,
    min(yelper.useful_votes) AS min_yelper_useful_votes,
    max(yelper.useful_votes) AS max_yelper_useful_votes,
    avg(yelper.years_elite) AS avg_yelper_years_elite,
    min(yelper.years_elite) AS min_yelper_years_elite,
    max(yelper.years_elite) AS max_yelper_years_elite 
FROM
    business_review_user_review other_review
    JOIN business ON business.business_id = other_review.first_business_id
    JOIN review ON review.review_id = other_review.other_review_id
    JOIN "user" yelper ON yelper.user_id = review.user_id
WHERE
    credit_cards is NOT NULL AND price_range IS NOT NULL 
GROUP BY business.business_id;         


